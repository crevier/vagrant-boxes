Debian Postgres
===============

**Name** : postgreSQL

**Usage** : For dev, only the host can access it.

**Based on box** : debian/jessie64

**Forwarded ports** :
 - Host `15432` -> Gest `5432`

Provisioning
------------

Run the [postgres_install.sh](postgres_install.sh) script :

Install a postgreSQL 11, with a `vagrant` database 
then create the `vagrant` user ( password : `vagrant` )
and finaly drop the default postgres database.